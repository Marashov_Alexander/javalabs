import { observable } from 'mobx';
import {PeopleItemModel} from "./PeopleModel";
import {SubjectsItemModel} from "./SubjectsModel";


export interface MarksItemModel {
  id: number;
  subject: SubjectsItemModel;
  student: PeopleItemModel;
  teacher: PeopleItemModel;
  value: number;
}

export class MarksModel {

  @observable
  isLoading: boolean = false;

  @observable
  error: string = null;

  @observable
  items: MarksItemModel[] = [];

}


export const marks = new MarksModel();

