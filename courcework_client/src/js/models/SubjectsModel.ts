import { observable } from 'mobx';


export interface SubjectsItemModel {
  id: number;
  name: string;
}

export class SubjectsModel {

  @observable
  isLoading: boolean = false;

  @observable
  error: string = null;

  @observable
  items: SubjectsItemModel[] = [];

}


export const subjects = new SubjectsModel();

