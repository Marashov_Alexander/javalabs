import { observable } from 'mobx';
import {GroupItemModel} from "./GroupsModel";


export interface PeopleItemModel {
  id: number;
  firstName: string;
  lastName: string;
  fatherName: string;
  group: GroupItemModel;
  status: string;
}

export class PeopleModel {

  @observable
  isLoading: boolean = false;

  @observable
  error: string = null;

  @observable
  items: PeopleItemModel[] = [];

  @observable
  isStudent: boolean = true;

}


export const people = new PeopleModel();

