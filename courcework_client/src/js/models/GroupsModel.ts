import { observable } from 'mobx';


export interface GroupItemModel {
  id: number;
  name: string;
}

export class GroupsModel {

  @observable
  isLoading: boolean = false;

  @observable
  error: string = null;

  @observable
  items: GroupItemModel[] = [];

}


export const groups = new GroupsModel();

