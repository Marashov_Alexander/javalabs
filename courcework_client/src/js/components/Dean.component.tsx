import React, { Component } from 'react';
import { observer } from 'mobx-react';
import {appState, State} from "../states";
import {MainScreenComponent} from "./MainScreen.component";
import {ErrorComponent} from "./Error.component";
import {AuthorizationScreenComponent} from "./Authorization.component";
import {StudentsActionsScreenComponent} from "./StudentsActions.component";
import {PeopleQueryScreenComponent} from "./people/PeopleQuery.component";
import {LoadingScreenComponent} from "./Loading.component";
import {SubjectsQueryScreenComponent} from "./subjects/SubectsQuery.component";
import {GroupsQueryScreenComponent} from "./groups/GroupsQuery.component";
import {MarksQueryScreenComponent} from "./marks/MarksQuery.component";
import {MarksActionsScreenComponent} from "./marks/MarksActions.component";


@observer
export class DeanComponent extends Component {

  // onAdd = (value: TodoItemAddingData) => {
    // addTodoAction(value);
  // };

  // componentDidMount(): void {
  //   getAllStudentsAction();
  // }

  render() {

    switch (appState.state) {
      case State.ERROR_SCREEN:
        return <ErrorComponent errorMessage={ appState.errorMessage } />;
      case State.LOADING_SCREEN:
        return <LoadingScreenComponent />;
      case State.MAIN_SCREEN:
        return <MainScreenComponent />;
      case State.AUTHORIZATION_SCREEN:
        return <AuthorizationScreenComponent />;
      case State.STUDENTS_ACTIONS_SCREEN:
        return <StudentsActionsScreenComponent />;
      case State.PEOPLE_QUERY_SCREEN:
        return <PeopleQueryScreenComponent  />;
      // case State.TEACHERS_ACTIONS_SCREEN:
      //   return <TeachersActionsScreenComponent />;
      // case State.TEACHERS_QUERY_SCREEN:
      //   return <TeachersQueryScreenComponent />;
      // case State.SUBJECTS_ACTIONS_SCREEN:
      //   return <SubjectsActionsScreenComponent />;
      case State.SUBJECTS_QUERY_SCREEN:
        return <SubjectsQueryScreenComponent />;
      case State.MARKS_ACTIONS_SCREEN:
        return <MarksActionsScreenComponent />;
      case State.MARKS_QUERY_SCREEN:
        return <MarksQueryScreenComponent />;
      // case State.GROUPS_ACTIONS_SCREEN:
      //   return <GroupsActionsScreenComponent />;
      case State.GROUPS_QUERY_SCREEN:
        return <GroupsQueryScreenComponent />;
      default:
        return <ErrorComponent errorMessage={"Unknown state o_O"} />;
    }

    // return <div className="dean">
    //   {
    //     stateSwitch()
    //   }
      {/*<MarksListComponent items={ peopleModel.items } />*/}
      {/*{*/}
      {/*  peopleModel.error != null*/}
      {/*    ? <div style={ {color: '#f00'} }>{ peopleModel.error }</div>*/}
      {/*    : <></>*/}
      {/*}*/}
      {/*{*/}
      {/*  peopleModel.isLoading*/}
      {/*    ? <div>isLoading...</div>*/}
      {/*    : <></>*/}
      {/*}*/}
      {/*{*/}
      {/*  !peopleModel.isLoading && peopleModel.error == null*/}
      {/*    ? <>*/}

      {/*    </>*/}
      {/*    : <>*/}
      {/*        <MarksListComponent items={ peopleModel.items } />*/}
      {/*      </>*/}
      {/*}*/}

    // </div>
  }

}
