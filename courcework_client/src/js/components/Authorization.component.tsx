import React, {Component} from "react";
import {loginAction} from "../actions/loginAction";

export class AuthorizationScreenComponent extends Component {
    render() {
        const logAction = () => {
            loginAction(
                (document.getElementById("login") as HTMLInputElement).value,
                (document.getElementById("password") as HTMLInputElement).value
            );
        };
        return <div className="AuthorizationComponent">
            <div>Please, log in</div>
            <input id="login" placeholder="Enter your login here"/>
            <input id="password" placeholder="Enter your password here"/>
            <button onClick={ logAction }>Ok</button>
        </div>
    }
}