import React, {Component} from "react";
import {changeState, State} from "../states";

export class ErrorComponent extends Component<{
    errorMessage: any,
}> {
    render() {

        const errorMessage = this.props.errorMessage;

        return <div className="ErrorComponent">
            An error occurred: { errorMessage }<br/>
            <button onClick={() => { changeState(State.MAIN_SCREEN) }}>Main screen</button>
        </div>
    }
}