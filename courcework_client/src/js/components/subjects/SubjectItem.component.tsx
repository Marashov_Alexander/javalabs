import React, { Component } from 'react';
import {removePeopleItemAction} from "../../actions/dean/remove/removePeopleItem.action";
import {changePeopleItemAction} from "../../actions/dean/update/changePeopleItem.action";
import {correctPeopleData} from "../people/PeopleAddItem.component";
import {PeopleItemAddingData} from "../../actions/dean/add/addPeople.action";
import {GroupSelectComponent} from "../groups/GroupSelect.component";
import {groups} from "../../models/GroupsModel";
import {SubjectsItemModel} from "../../models/SubjectsModel";
import {removeSubjectItemAction} from "../../actions/dean/remove/removeSubjectItem.action";
import {SubjectItemAddingData} from "../../actions/dean/add/addSubject.action";
import {correctSubjectData} from "./SubjectsAddItem.component";
import {changeSubjectItemAction} from "../../actions/dean/update/changeSubjectItem.action";

export var itemChanged: boolean = false;

export class SubjectItemComponent extends Component<{
  itemData: SubjectsItemModel,
}> {

    onChange = () => {
        itemChanged = true;
    };

  onDelete = () => {
      removeSubjectItemAction(this.props.itemData.id);
  };

  onUpdate = () => {
      if (!itemChanged) {
          return;
      }
      itemChanged = false;
      let subjectData: SubjectItemAddingData;
      subjectData = {
          name: (document.getElementById("subject-item_name" + this.props.itemData.id) as HTMLInputElement).value,
      };

      if (correctSubjectData(subjectData)) {
          changeSubjectItemAction(this.props.itemData.id, subjectData);
      }
  };

  render() {
    const itemData = this.props.itemData;

    const itemStyle =
        {
          'display': 'flex',
          'flex-direction': 'row',
        };

    return <div className="subject-item" style={ itemStyle }>
      {/*<input*/}
      {/*    id={"subject-item_id" + itemData.id}*/}
      {/*    defaultValue={ itemData.id }*/}
      {/*    style={ {*/}
      {/*        'width': '5%',*/}
      {/*    } }*/}
      {/*/>*/}
      <input
          id={"subject-item_name" + itemData.id}
        defaultValue={ itemData.name }
        style={ {
          'width': '23%',
        } }
            onChange={ this.onChange }
            onBlur={ this.onUpdate }
        />
      <button
        className="subject-item_delete"
        aria-label="Delete subject"
        onClick={ this.onDelete }
        style={ {
          'width': '3%',
        } }
      >×</button>
    </div>
  }

}
