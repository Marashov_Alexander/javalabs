import React, { Component } from 'react';
import { PeopleItemModel } from '../../models/PeopleModel';
import {PeopleItemComponent} from "../people/PeopleItem.component";
import {SubjectsItemModel} from "../../models/SubjectsModel";
import {SubjectItemComponent} from "./SubjectItem.component";


interface SubjectsListComponentProps {
  items: Array<SubjectsItemModel>,
}


export class SubjectsListComponent extends Component<SubjectsListComponentProps> {

  render() {
    return <div className="subjects-list">
      <h1>Queried subjects</h1>
      <div className="subjects-list_header" style={ {display: 'flex'} }>
        {/*<div style={ {width: '5%', textAlign: "center"} }>Id</div>*/}
        <div style={ {width: '23%', textAlign: "center"} }>Name</div>
        <div style={ {width: '11%', textAlign: "center"} }> </div>
      </div>
      {
        this.props.items != null
            ? this.props.items.map(itemData =>
                <SubjectItemComponent itemData={ itemData }/>
              )
            : ''
      }
    </div>;
  }

}
