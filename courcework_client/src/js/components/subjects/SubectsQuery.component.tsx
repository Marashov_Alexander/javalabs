import React, {Component} from "react";
import {PeopleListComponent} from "../people/PeopleList.component";
import {people} from "../../models/PeopleModel";
import {observer} from "mobx-react";
import {changeState, State} from "../../states";
import {PeopleAddItemComponent} from "../people/PeopleAddItem.component";
import {subjects} from "../../models/SubjectsModel";
import {SubjectAddItemComponent} from "./SubjectsAddItem.component";
import {SubjectsListComponent} from "./SubjectsList.component";

@observer
export class SubjectsQueryScreenComponent extends Component {
    render() {
        return <div className="SubjectsQueryComponent">
            <SubjectAddItemComponent />
            <br/>
            <SubjectsListComponent items={ subjects.items }/>
            <br/>
            <button onClick={() => changeState(State.MAIN_SCREEN)}>Main menu</button><br/>
        </div>
    }
}