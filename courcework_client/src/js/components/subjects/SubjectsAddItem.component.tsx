import React, {Component} from "react";
import {addPeopleAction, PeopleItemAddingData} from "../../actions/dean/add/addPeople.action";
import {GroupSelectComponent} from "../groups/GroupSelect.component";
import {groups} from "../../models/GroupsModel";
import {SubjectsItemModel} from "../../models/SubjectsModel";
import {addSubjectAction, SubjectItemAddingData} from "../../actions/dean/add/addSubject.action";

export function correctSubjectData(subjectData: SubjectItemAddingData) {
    if (subjectData.name == '') {
        alert("Invalid name");
        return false;
    }
    return true;
}

function tryGetSubjectData() {

    let subjectData: SubjectItemAddingData;
    subjectData = {
        name: (document.getElementById("NameInput") as HTMLInputElement).value,
    };

    if (correctSubjectData(subjectData)) {
        return subjectData;
    } else {
        return null;
    }
}

export class SubjectAddItemComponent extends Component {
    render() {

        return <div >
            <h1>Adding new subject</h1>
            <div style={
                {
                    display: "flex"
                }
            }>
                <input
                    id="NameInput"
                    placeholder="Enter subject name"
                    style={{
                        width: "20%"
                    }}
                />
                <button
                    style={{
                        width: "10%"
                    }}
                    onClick={ () => addSubjectAction(tryGetSubjectData()) }
                >add</button>
            </div>
        </div>
    }
}