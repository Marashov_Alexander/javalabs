import React, {Component} from "react";
import {errorCaused} from "../../states";
import {SubjectsItemModel} from "../../models/SubjectsModel";

interface SubjectListComponentProps {
    idString: string,
    items: Array<SubjectsItemModel>,
    defaultSubjectId: number,
    onChange: () => void
}

export var chosenSubjectId: number = 0;

export class SubjectSelectComponent extends Component<SubjectListComponentProps> {
    componentDidMount(): void {
        let list = (document.getElementById(this.props.idString) as HTMLSelectElement).options;

        if (this.props.defaultSubjectId == 0) {
            list.selectedIndex = 0;
            return;
        }

        list[list.selectedIndex].selected = false;
        for (let i = 0; i < list.length; ++i) {
            if (list.item(i).value as unknown as number == this.props.defaultSubjectId) {
                list.item(i).selected = true;
                chosenSubjectId = this.props.defaultSubjectId;
                break;
            }
        }
    }

    changeSelected = () => {
        try {
            chosenSubjectId = (document.getElementById(this.props.idString) as HTMLSelectElement).selectedOptions[0].value as unknown as number;
        } catch (e) {
            errorCaused(e.toString());
            return;
        }
        this.props.onChange()
    };

    render() {
        return <select onChange={ this.changeSelected } id={ this.props.idString }>
            {/*<option disabled selected value={0}>Group</option>*/}
            <option value={0}>No subject</option>
            {
                this.props.items != null
                        ? this.props.items.map(item => {
                            return <option value={ item.id }>{ item.name }</option>;
                          })
                        : ''
            }
        </select>
    }
}