import React, {Component} from "react";
import {changeState, State} from "../states";
import {
    getPeopleAction,
    getStudentsByGroupAction,
    getStudentsByMarkAction,
    getStudentsBySubjectAction
} from "../actions/dean/get/getPeopleAction";
import {buttonStyle, menuStyle} from "../styles";
import {chosenGroupId, GroupSelectComponent} from "./groups/GroupSelect.component";
import {groups} from "../models/GroupsModel";
import {chosenSubjectId, SubjectSelectComponent} from "./subjects/SubjectSelect.component";
import {subjects} from "../models/SubjectsModel";

export class StudentsActionsScreenComponent extends Component {

    render() {

        function getMarks() {
            let minMark: number = 0;
            let maxMark: number = 0;
            try {
                minMark = (document.getElementById("minMarkInput") as HTMLInputElement).value as unknown as number;
                maxMark = (document.getElementById("maxMarkInput") as HTMLInputElement).value as unknown as number;
                if (minMark > maxMark || minMark < 1 || maxMark > 5) {
                    throw '';
                }
            } catch (e) {
                alert("invalid marks");
                return null;
            }
            return {
                minMark: minMark,
                maxMark: maxMark
            };
        }

        return <div className="StudentsActionsComponent" style={ menuStyle }>
            <button style={ buttonStyle } onClick={ () => { getPeopleAction(true); } } >All</button>
            <div style={ buttonStyle }>
                <GroupSelectComponent
                    idString="selectGroup"
                    items={ groups.items}
                    defaultGroupId={ chosenGroupId }
                    onChange={ () => {} }
                />
                <button style={ buttonStyle } onClick={ () => { getStudentsByGroupAction(chosenGroupId) } } >By group</button>
            </div>
            <div style={ buttonStyle }>
                <input id="minMarkInput" placeholder={"min mark"}/>
                <input id="maxMarkInput" placeholder={"max mark"}/>
                <button style={ buttonStyle } onClick={ () => {
                    const tmp = getMarks();
                    if (tmp == null) {
                        return;
                    }
                    getStudentsByMarkAction(tmp.minMark, tmp.maxMark);
                } } >By mark</button>
            </div>
            <div style={ buttonStyle }>
                <SubjectSelectComponent
                    idString="selectSubject"
                    items={ subjects.items }
                    defaultSubjectId={ chosenSubjectId }
                    onChange={ () => {} }
                />
                <button style={ buttonStyle } onClick={ () => { getStudentsBySubjectAction(chosenSubjectId) } } >By subject</button>
            </div>
            <button style={ buttonStyle } onClick={ () => { changeState(State.MAIN_SCREEN) } } >Main menu</button>
        </div>
    }
}