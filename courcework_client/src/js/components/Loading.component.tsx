import React, {Component} from "react";

export class LoadingScreenComponent extends Component {
    render() {
        return <div className="LoadingComponent">
            Loading...
        </div>
    }
}