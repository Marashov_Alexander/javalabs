import React, {Component} from "react";
import {State} from "../states";
import {getPeopleAction} from "../actions/dean/get/getPeopleAction";
import {buttonStyle, menuStyle} from "../styles";
import {getGroupsAction} from "../actions/dean/get/getGroupsAction";
import {getSubjectsAction} from "../actions/dean/get/getSubjectsAction";
import {getMarksAction} from "../actions/dean/get/getMarksAction";
import {PeopleSelectComponent} from "./people/PeopleSelect.component";

export class MainScreenComponent extends Component {
    render() {

        return <div className="MainScreen" style={ menuStyle }>
            <button style={ buttonStyle } onClick={ () => {
                getGroupsAction(State.STUDENTS_ACTIONS_SCREEN);
            }} >Students</button>
            <button style={ buttonStyle } onClick={ () => getPeopleAction(false) } >Teachers</button>
            <button style={ buttonStyle } onClick={ () => getGroupsAction() } >Groups</button>
            <button style={ buttonStyle } onClick={ () => {
                getPeopleAction(undefined, State.LOADING_SCREEN).then(() => {
                    getSubjectsAction(State.MARKS_ACTIONS_SCREEN);
                });
            } } >Marks</button>
            <button style={ buttonStyle } onClick={ () => getSubjectsAction() } >Subjects</button>
        </div>
    }
}