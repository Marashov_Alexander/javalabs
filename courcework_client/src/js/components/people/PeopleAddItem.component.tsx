import React, {Component} from "react";
import {addPeopleAction, PeopleItemAddingData} from "../../actions/dean/add/addPeople.action";
import {GroupSelectComponent} from "../groups/GroupSelect.component";
import {groups} from "../../models/GroupsModel";

export function correctPeopleData(peopleData: PeopleItemAddingData) {
    if (peopleData.groupId == '0') {
        peopleData.groupId = null;
    }
    try {
        if (peopleData.firstName == '') {
            throw 'Invalid name';
        }
        if (peopleData.lastName == '') {
            throw 'Invalid last name';
        }
        if (peopleData.status == 't' && peopleData.groupId != null) {
            throw 'Teacher should not have a group';
        }
        if (peopleData.status == 's' && peopleData.groupId == null) {
            throw 'Student should have a group'
        }
    } catch (e) {
        alert("Invalid data: " + e);
        return false;
    }
    return true;
}

function tryGetPeopleData() {

    let peopleData: PeopleItemAddingData;
    peopleData = {
        firstName: (document.getElementById("FirstNameInput") as HTMLInputElement).value,
        lastName: (document.getElementById("LastNameInput") as HTMLInputElement).value,
        fatherName: (document.getElementById("FatherNameInput") as HTMLInputElement).value,
        groupId: (document.getElementById("GroupIdInput") as HTMLSelectElement).selectedOptions[0].value,
        status: ((document.getElementById("IsTeacherCheckbox") as HTMLInputElement).checked as boolean) ? 't' : 's' ,
    };

    if (correctPeopleData(peopleData)) {
        return peopleData;
    } else {
        return null;
    }
}

export class PeopleAddItemComponent extends Component {
    render() {

        return <div >
            <h1>Adding new person</h1>
            <div style={
                {
                    display: "flex"
                }
            }>
                <input
                    id="FirstNameInput"
                    placeholder="Enter name"
                    style={{
                        width: "20%"
                    }}
                />
                <input
                    id="LastNameInput"
                    placeholder="Enter last name"
                    style={{
                        width: "20%"
                    }}
                />
                <input
                    id="FatherNameInput"
                    placeholder="Enter father name"
                    style={{
                        width: "20%"
                    }}
                />
                <GroupSelectComponent
                    idString="GroupIdInput"
                    items={ groups.items }
                    defaultGroupId={ 0 }
                    onChange={ () => {} }
                />
                <div
                    style={{
                        width: "7%"
                    }}
                >Teacher: </div>
                <input
                    id="IsTeacherCheckbox"
                    type="checkbox"
                    style={{
                        width: "2%"
                    }}
                />
                <button
                    style={{
                        width: "10%"
                    }}
                    onClick={ () => addPeopleAction(tryGetPeopleData()) }
                >add</button>
            </div>
        </div>
    }
}