import React, {Component} from "react";
import {PeopleListComponent} from "./PeopleList.component";
import {people} from "../../models/PeopleModel";
import {observer} from "mobx-react";
import {changeState, State} from "../../states";
import {PeopleAddItemComponent} from "./PeopleAddItem.component";

@observer
export class PeopleQueryScreenComponent extends Component {
    render() {
        return <div className="PeopleQueryComponent">
            <PeopleAddItemComponent />
            <br/>
            <PeopleListComponent items={ people.items }/>
            <br/>
            <button onClick={() => changeState(State.STUDENTS_ACTIONS_SCREEN)} >Students actions</button><br/>
            <button onClick={() => changeState(State.MAIN_SCREEN)}>Main menu</button><br/>
        </div>
    }
}