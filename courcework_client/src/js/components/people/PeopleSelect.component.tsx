import React, {Component} from "react";
import {GroupItemModel} from "../../models/GroupsModel";
import {errorCaused} from "../../states";
import {PeopleItemModel} from "../../models/PeopleModel";

interface PeopleListComponentProps {
    idString: string,
    items: Array<PeopleItemModel>,
    defaultPeopleId: number,
    onChange: () => void
}

export var chosenPeopleId: number = 0;

export class PeopleSelectComponent extends Component<PeopleListComponentProps> {

    componentDidMount(): void {

        let list = (document.getElementById(this.props.idString) as HTMLSelectElement).options;

        if (this.props.defaultPeopleId == 0) {
            list.selectedIndex = 0;
            return;
        }

        list[list.selectedIndex].selected = false;
        for (let i = 0; i < list.length; ++i) {
            if (list.item(i).value as unknown as number == this.props.defaultPeopleId) {
                list.item(i).selected = true;
                chosenPeopleId = this.props.defaultPeopleId;
                break;
            }
        }
    }

    changeSelected = () => {
        try {
            chosenPeopleId = (document.getElementById(this.props.idString) as HTMLSelectElement).selectedOptions[0].value as unknown as number;
        } catch (e) {
            errorCaused(e.toString());
            return;
        }
        this.props.onChange()
    };

    render() {
        return <select onChange={ this.changeSelected } id={ this.props.idString }>
            <option value={0}>No people</option>
            {
                this.props.items != null
                        ? this.props.items.map(item => {
                            return <option value={ item.id }>{ item.firstName + '_' + item.lastName + '_' + item.fatherName + '_' + (item.group == null ? '' : item.group.name )}</option>;
                          })
                        : ''
            }
        </select>
    }


}