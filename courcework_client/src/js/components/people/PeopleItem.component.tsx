import React, { Component } from 'react';
import { PeopleItemModel } from '../../models/PeopleModel';
import {removePeopleItemAction} from "../../actions/dean/remove/removePeopleItem.action";
import {changePeopleItemAction} from "../../actions/dean/update/changePeopleItem.action";
import {correctPeopleData} from "./PeopleAddItem.component";
import {PeopleItemAddingData} from "../../actions/dean/add/addPeople.action";
import {GroupSelectComponent} from "../groups/GroupSelect.component";
import {groups} from "../../models/GroupsModel";

export var itemChanged: boolean = false;

export class PeopleItemComponent extends Component<{
  itemData: PeopleItemModel,
}> {

    onChange = () => {
        itemChanged = true;
    };

  onDelete = () => {
      removePeopleItemAction(this.props.itemData.id);
  };

  onUpdate = () => {
      if (!itemChanged) {
          return;
      }
      itemChanged = false;
      let peopleData: PeopleItemAddingData;
      peopleData = {
          firstName: (document.getElementById("people-item_first-name" + this.props.itemData.id) as HTMLInputElement).value,
          lastName: (document.getElementById("people-item_last-name" + this.props.itemData.id) as HTMLInputElement).value,
          fatherName: (document.getElementById("people-item_father-name" + this.props.itemData.id) as HTMLInputElement).value,
          groupId: (document.getElementById("people-item_group-id" + this.props.itemData.id) as HTMLSelectElement).selectedOptions[0].value,
          status: ((document.getElementById("people-item_is-teacher" + this.props.itemData.id) as HTMLInputElement).checked as boolean) ? 't' : 's' ,
      };

      if (correctPeopleData(peopleData)) {
          changePeopleItemAction(this.props.itemData.id, peopleData);
      }
  };

  render() {
    const itemData = this.props.itemData;

    const itemStyle =
        {
          'display': 'flex',
          'flex-direction': 'row',
        };

    return <div className="people-item" style={ itemStyle }>
      {/*<input*/}
      {/*    id={"people-item_id" + itemData.id}*/}
      {/*    defaultValue={ itemData.id }*/}
      {/*    style={ {*/}
      {/*        'width': '5%',*/}
      {/*    } }*/}
      {/*/>*/}
      <input
          id={"people-item_first-name" + itemData.id}
        defaultValue={ itemData.firstName }
        style={ {
          'width': '23%',
        } }
            onChange={ this.onChange }
            onBlur={ this.onUpdate }
        />
      <input
          id={"people-item_last-name" + itemData.id}
          defaultValue={ itemData.lastName }
          style={ {
            'width': '23%',
          } }
          onChange={ this.onChange }
          onBlur={ this.onUpdate }
      />
      <input
          id={"people-item_father-name" + itemData.id}
          defaultValue={ itemData.fatherName }
          style={ {
            'width': '23%',
          } }
          onChange={ this.onChange }
          onBlur={ this.onUpdate }
      />
      <GroupSelectComponent
          idString={"people-item_group-id" + itemData.id}
          items={ groups.items }
          defaultGroupId={ itemData.group != null ? itemData.group.id : 0 }
          onChange={ () => {
              this.onChange();
              this.onUpdate();
          } }
      />
      <input
          type="checkbox"
          id={"people-item_is-teacher" + itemData.id}
          defaultChecked={ itemData.status === 't' }
          style={ {
            'width': '8%',
          } }
          onChange={ () => {
              this.onChange();
              this.onUpdate();
          } }
      />
      <button
        className="people-item_delete"
        aria-label="Delete people"
        onClick={ this.onDelete }
        style={ {
          'width': '3%',
        } }
      >×</button>
    </div>
  }

}
