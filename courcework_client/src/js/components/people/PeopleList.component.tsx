import React, { Component } from 'react';
import { PeopleItemModel } from '../../models/PeopleModel';
import {PeopleItemComponent} from "./PeopleItem.component";


interface PeopleListComponentProps {
  items: Array<PeopleItemModel>,
}


export class PeopleListComponent extends Component<PeopleListComponentProps> {

  render() {
    return <div className="people-list">
      <h1>Queried persons</h1>
      <div className="people-list_header" style={ {display: 'flex'} }>
        {/*<div style={ {width: '5%', textAlign: "center"} }>Id</div>*/}
        <div style={ {width: '23%', textAlign: "center"} }>First name</div>
        <div style={ {width: '23%', textAlign: "center"} }>Last name</div>
        <div style={ {width: '23%', textAlign: "center"} }>Father name</div>
        <div style={ {width: '15%', textAlign: "center"} }>Group</div>
        <div style={ {width: '11%', textAlign: "center"} }> </div>
      </div>
      {
        this.props.items != null
            ? this.props.items.map(itemData =>
                <PeopleItemComponent itemData={ itemData }/>
              )
            : ''
      }
    </div>;
  }

}
