import React, {Component} from "react";
import {SubjectSelectComponent} from "../subjects/SubjectSelect.component";
import {subjects} from "../../models/SubjectsModel";
import {PeopleSelectComponent} from "../people/PeopleSelect.component";
import {people} from "../../models/PeopleModel";
import {addMarkAction, MarkItemAddingData} from "../../actions/dean/add/addMark.action";

export function correctMarkData(markData: MarkItemAddingData) {
    try {
        if (markData.subjectId == '0') {
            throw 'Invalid subject';
        }
        if (markData.teacherId == '0') {
            throw 'Invalid teacher';
        }
        if (markData.studentId == '0') {
            throw 'Invalid student';
        }
    } catch (e) {
        alert("Invalid data: " + e);
        return false;
    }
    return true;
}

function tryGetMarkData() {

    let markData: MarkItemAddingData;
    markData = {
        studentId: (document.getElementById("StudentIdInput") as HTMLSelectElement).selectedOptions[0].value,
        subjectId: (document.getElementById("SubjectIdInput") as HTMLSelectElement).selectedOptions[0].value,
        teacherId: (document.getElementById("TeacherIdInput") as HTMLSelectElement).selectedOptions[0].value,
        value: (document.getElementById("ValueInput") as HTMLSelectElement).selectedOptions[0].value
    };

    if (correctMarkData(markData)) {
        return markData;
    } else {
        return null;
    }
}

export const peopleComp = (a, b) => {
    if (a.firstName > b.firstName) {
        return 1;
    } else if (a.firstName < b.firstName) {
        return -1;
    } else if (a.lastName > b.lastName) {
        return 1;
    } else if (a.lastName < b.lastName) {
        return -1;
    } else if (a.fatherName > b.fatherName) {
        return 1;
    } else if (a.fatherName < b.fatherName) {
        return -1;
    } else {
        return 0;
    }
};

export class MarkAddItemComponent extends Component {

    render() {

        return <div >
            <h1>Adding new mark</h1>
            <div style={
                {
                    display: "flex"
                }
            }>
                <SubjectSelectComponent
                    idString="SubjectIdInput"
                    items={ subjects.items }
                    defaultSubjectId={ 0 }
                    onChange={ () => {} }
                />

                <PeopleSelectComponent
                    idString="StudentIdInput"
                    items={ people.items.filter(student => { return student.status === 's' }).sort(peopleComp) }
                    defaultPeopleId={ 0 }
                    onChange={ () => {} }
                />

                <PeopleSelectComponent
                    idString="TeacherIdInput"
                    items={ people.items.filter(student => { return student.status === 't' }).sort(peopleComp) }
                    defaultPeopleId={ 0 }
                    onChange={ () => {} }
                />
                <select id={"ValueInput"}>
                    <option selected>5</option>
                    <option>4</option>
                    <option>3</option>
                    <option>2</option>
                    <option>1</option>
                </select>
                <button
                    style={{
                        width: "10%"
                    }}
                    onClick={ () => addMarkAction(tryGetMarkData()) }
                >add</button>
            </div>
        </div>
    }
}