import React, { Component } from 'react';
import {MarkItemComponent} from "./MarkItem.component";
import {MarksItemModel} from "../../models/MarksModel";


interface PeopleListComponentProps {
  items: Array<MarksItemModel>
}


export class MarksListComponent extends Component<PeopleListComponentProps> {

  render() {
    return <div className="marks-list">
      <h1>Queried marks</h1>
      <div className="people-list_header" style={ {display: 'flex'} }>
        {/*<div style={ {width: '5%', textAlign: "center"} }>Id</div>*/}
        <div style={ {width: '8%', textAlign: "center"} }>Subject name</div>
        <div style={ {width: '22%', textAlign: "center"} }>Student</div>
        <div style={ {width: '15%', textAlign: "center"} }>Teacher</div>
        <div style={ {width: '5%', textAlign: "center"} }>Value</div>
        <div style={ {width: '0%', textAlign: "center"} }> </div>
      </div>
      {
        this.props.items != null
            ? this.props.items.map(itemData =>
                <MarkItemComponent itemData={ itemData }/>
              )
            : ''
      }
    </div>;
  }

}
