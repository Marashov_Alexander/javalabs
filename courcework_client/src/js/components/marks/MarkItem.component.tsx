import React, { Component } from 'react';
import {people} from '../../models/PeopleModel';
import {correctMarkData} from "./MarkAddItem.component";
import {MarksItemModel} from "../../models/MarksModel";
import {removeMarkItemAction} from "../../actions/dean/remove/removeMarkItem.action";
import {MarkItemAddingData} from "../../actions/dean/add/addMark.action";
import {SubjectSelectComponent} from "../subjects/SubjectSelect.component";
import {subjects} from "../../models/SubjectsModel";
import {PeopleSelectComponent} from "../people/PeopleSelect.component";
import {changeMarkItemAction} from "../../actions/dean/update/changeMarkItem.action";

export var itemChanged: boolean = false;

export class MarkItemComponent extends Component<{
  itemData: MarksItemModel,
}> {
    onChange = () => {
        itemChanged = true;
    };

  onDelete = () => {
      removeMarkItemAction(this.props.itemData.id);
  };

  onUpdate = () => {
      if (!itemChanged) {
          return;
      }
      itemChanged = false;
      let markData: MarkItemAddingData;
      markData = {
          studentId: (document.getElementById("mark-item_student-id" + this.props.itemData.id) as HTMLSelectElement).selectedOptions[0].value,
          teacherId: (document.getElementById("mark-item_teacher-id" + this.props.itemData.id) as HTMLSelectElement).selectedOptions[0].value,
          subjectId: (document.getElementById("mark-item_subject-id" + this.props.itemData.id) as HTMLSelectElement).selectedOptions[0].value,
          value: (document.getElementById("mark-item_value" + this.props.itemData.id) as HTMLSelectElement).selectedOptions[0].value
      };

      if (correctMarkData(markData)) {
          changeMarkItemAction(this.props.itemData.id, markData);
      }
  };

  render() {
    const itemData = this.props.itemData;

    const itemStyle =
        {
          'display': 'flex',
          'flex-direction': 'row',
        };

    return <div className="mark-item" style={ itemStyle }>
      {/*<input*/}
      {/*    id={"mark-item_id" + itemData.id}*/}
      {/*    defaultValue={ itemData.id }*/}
      {/*    style={ {*/}
      {/*        'width': '5%',*/}
      {/*    } }*/}
      {/*/>*/}

        <SubjectSelectComponent
            idString={"mark-item_subject-id" + this.props.itemData.id}
            items={ subjects.items }
            defaultSubjectId={ itemData.subject != null ? itemData.subject.id : 0 }
            onChange={ () => {
                this.onChange();
                this.onUpdate()
            } }
        />

        <PeopleSelectComponent
            idString={"mark-item_student-id" + this.props.itemData.id}
            items={ people.items.filter(student => { return student.status === 's' }) }
            defaultPeopleId={ itemData.student != null ? itemData.student.id : 0 }
            onChange={ () => {
                this.onChange();
                this.onUpdate()
            } }
        />

        <PeopleSelectComponent
            idString={"mark-item_teacher-id" + this.props.itemData.id}
            items={ people.items.filter(student => { return student.status === 't' }) }
            defaultPeopleId={ itemData.teacher != null ? itemData.teacher.id : 0 }
            onChange={ () => {
                this.onChange();
                this.onUpdate()
            } }
        />
        <select id={"mark-item_value" + this.props.itemData.id} onChange={() => {
            this.onChange();
            this.onUpdate()
        }} defaultValue={this.props.itemData.value}>
            <option>5</option>
            <option>4</option>
            <option>3</option>
            <option>2</option>
            <option>1</option>
        </select>


        <button
        className="people-item_delete"
        aria-label="Delete people"
        onClick={ this.onDelete }
        style={ {
          'width': '3%',
        } }
      >×</button>
    </div>
  }

}
