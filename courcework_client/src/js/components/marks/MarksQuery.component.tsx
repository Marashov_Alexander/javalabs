import React, {Component} from "react";
import {MarksListComponent} from "./MarksList.component";
import {people} from "../../models/PeopleModel";
import {observer} from "mobx-react";
import {changeState, State} from "../../states";
import {MarkAddItemComponent} from "./MarkAddItem.component";
import {marks} from "../../models/MarksModel";

@observer
export class MarksQueryScreenComponent extends Component {
    render() {
        return <div className="MarksQueryComponent">
            <MarkAddItemComponent />
            <br/>
            <MarksListComponent items={ marks.items }/>
            <br/>
            <button onClick={() => changeState(State.MAIN_SCREEN)}>Main menu</button><br/>
        </div>
    }
}