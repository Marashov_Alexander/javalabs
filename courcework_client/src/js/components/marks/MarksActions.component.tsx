import React, {Component} from "react";
import {PeopleSelectComponent} from "../people/PeopleSelect.component";
import {people} from "../../models/PeopleModel";
import {getMarksAction} from "../../actions/dean/get/getMarksAction";
import {changeState, State} from "../../states";
import {peopleComp} from "./MarkAddItem.component";

export class MarksActionsScreenComponent extends Component {
    render() {

        const getStudentId = () => {
            return (document.getElementById("PeopleSelectComponent") as HTMLSelectElement).selectedOptions[0].value as unknown as number;
        };

        return <div>
            <PeopleSelectComponent
                idString={"PeopleSelectComponent"}
                items={ people.items.filter(item => {
                    return item.status === 's';
                }).sort(peopleComp) }
                onChange={() => {}}
                defaultPeopleId={ 0 }
            />
            <button onClick={() => {
                const studentId: number = getStudentId();
                if (studentId == 0) {
                    alert("Choose the student");
                    return;
                } else {
                    getMarksAction(getStudentId());
                }
            }}>Ok</button>
            <button onClick={() => {changeState(State.MAIN_SCREEN)}}>Main menu</button>
        </div>
    }
}