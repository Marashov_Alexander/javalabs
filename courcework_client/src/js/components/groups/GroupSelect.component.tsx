import React, {Component} from "react";
import {GroupItemModel} from "../../models/GroupsModel";
import {errorCaused} from "../../states";

interface GroupListComponentProps {
    idString: string,
    items: Array<GroupItemModel>,
    defaultGroupId: number,
    onChange: () => void
}

export var chosenGroupId: number = 0;

export class GroupSelectComponent extends Component<GroupListComponentProps> {
    componentDidMount(): void {
        let list = (document.getElementById(this.props.idString) as HTMLSelectElement).options;

        if (this.props.defaultGroupId == 0) {
            list.selectedIndex = 0;
            return;
        }

        list[list.selectedIndex].selected = false;
        for (let i = 0; i < list.length; ++i) {
            if (list.item(i).value as unknown as number == this.props.defaultGroupId) {
                list.item(i).selected = true;
                chosenGroupId = this.props.defaultGroupId;
                break;
            }
        }
    }

    changeSelected = () => {
        try {
            chosenGroupId = (document.getElementById(this.props.idString) as HTMLSelectElement).selectedOptions[0].value as unknown as number;
        } catch (e) {
            errorCaused(e.toString());
            return;
        }
        this.props.onChange()
    };

    render() {
        return <select onChange={ this.changeSelected } id={ this.props.idString }>
            {/*<option disabled selected value={0}>Group</option>*/}
            <option value={0}>No group</option>
            {
                this.props.items != null
                        ? this.props.items.map(item => {
                            return <option value={ item.id }>{ item.name }</option>;
                          })
                        : ''
            }
        </select>
    }
}