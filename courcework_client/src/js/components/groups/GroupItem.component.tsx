import React, { Component } from 'react';
import {GroupItemModel} from "../../models/GroupsModel";
import {removeGroupItemAction} from "../../actions/dean/remove/removeGroupItem.action";
import {GroupItemAddingData} from "../../actions/dean/add/addGroup.action";
import {correctGroupData} from "./GroupsAddItem.component";
import {changeGroupItemAction} from "../../actions/dean/update/changeGroupItem.action";

export var itemChanged: boolean = false;

export class GroupItemComponent extends Component<{
  itemData: GroupItemModel,
}> {

    onChange = () => {
        itemChanged = true;
    };

  onDelete = () => {
      removeGroupItemAction(this.props.itemData.id);
  };

  onUpdate = () => {
      if (!itemChanged) {
          return;
      }
      itemChanged = false;
      let groupData: GroupItemAddingData;
      groupData = {
          name: (document.getElementById("group-item_name" + this.props.itemData.id) as HTMLInputElement).value,
      };

      if (correctGroupData(groupData)) {
          changeGroupItemAction(this.props.itemData.id, groupData);
      }
  };

  render() {
    const itemData = this.props.itemData;

    const itemStyle =
        {
          'display': 'flex',
          'flex-direction': 'row',
        };

    return <div className="group-item" style={ itemStyle }>
      {/*<input*/}
      {/*    id={"group-item_id" + itemData.id}*/}
      {/*    defaultValue={ itemData.id }*/}
      {/*    style={ {*/}
      {/*        'width': '5%',*/}
      {/*    } }*/}
      {/*/>*/}
      <input
          id={"group-item_name" + itemData.id}
        defaultValue={ itemData.name }
        style={ {
          'width': '23%',
        } }
            onChange={ this.onChange }
            onBlur={ this.onUpdate }
        />
      <button
        className="group-item_delete"
        aria-label="Delete group"
        onClick={ this.onDelete }
        style={ {
          'width': '3%',
        } }
      >×</button>
    </div>
  }

}
