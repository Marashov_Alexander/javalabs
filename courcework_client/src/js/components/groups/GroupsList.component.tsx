import React, { Component } from 'react';
import { PeopleItemModel } from '../../models/PeopleModel';
import {PeopleItemComponent} from "../people/PeopleItem.component";
import {SubjectsItemModel} from "../../models/SubjectsModel";
import {GroupItemComponent} from "./GroupItem.component";
import {GroupItemModel} from "../../models/GroupsModel";


interface GroupsListComponentProps {
  items: Array<GroupItemModel>,
}


export class GroupsListComponent extends Component<GroupsListComponentProps> {

  render() {
    return <div className="subjects-list">
      <h1>Queried groups</h1>
      <div className="groups-list_header" style={ {display: 'flex'} }>
        {/*<div style={ {width: '5%', textAlign: "center"} }>Id</div>*/}
        <div style={ {width: '23%', textAlign: "center"} }>Name</div>
        <div style={ {width: '11%', textAlign: "center"} }> </div>
      </div>
      {
        this.props.items != null
            ? this.props.items.map(itemData =>
                <GroupItemComponent itemData={ itemData }/>
              )
            : ''
      }
    </div>;
  }

}
