import React, {Component} from "react";
import {addGroupAction, GroupItemAddingData} from "../../actions/dean/add/addGroup.action";

export function correctGroupData(groupData: GroupItemAddingData) {
    if (groupData.name == '') {
        alert("Invalid name");
        return false;
    }
    return true;
}

function tryGetGroupData() {

    let groupData: GroupItemAddingData;
    groupData = {
        name: (document.getElementById("GroupNameInput") as HTMLInputElement).value,
    };

    if (correctGroupData(groupData)) {
        return groupData;
    } else {
        return null;
    }
}

export class GroupsAddItemComponent extends Component {
    render() {

        return <div >
            <h1>Adding new group</h1>
            <div style={
                {
                    display: "flex"
                }
            }>
                <input
                    id="GroupNameInput"
                    placeholder="Enter group name"
                    style={{
                        width: "20%"
                    }}
                />
                <button
                    style={{
                        width: "10%"
                    }}
                    onClick={ () => addGroupAction(tryGetGroupData()) }
                >add</button>
            </div>
        </div>
    }
}