import React, {Component} from "react";
import {observer} from "mobx-react";
import {changeState, State} from "../../states";
import {GroupsListComponent} from "./GroupsList.component";
import {groups} from "../../models/GroupsModel";
import {GroupsAddItemComponent} from "./GroupsAddItem.component";
@observer
export class GroupsQueryScreenComponent extends Component {
    render() {
        return <div className="SubjectsQueryComponent">
            <GroupsAddItemComponent />
            <br/>
            <GroupsListComponent items={ groups.items }/>
            <br/>
            <button onClick={() => changeState(State.MAIN_SCREEN)}>Main menu</button><br/>
        </div>
    }
}