import {changeState, State} from "../states";

class HttpService {
  get password(): string {
    return this._password;
  }
  get login(): string {
    return this._login;
  }
  set password(value: string) {
    this._password = value;
  }
  set login(value: string) {
    this._login = value;
  }

  private _login: string;
  private _password: string;

  request<T>(
    method: string,
    url: string,
    data?: any,
  ): Promise<T> {
    return new Promise<any>((resolve, reject) => {
      const request = new XMLHttpRequest();

      request.open(method, url);

      request.addEventListener('readystatechange', () => {
        if (request.readyState === 4) {
          if (request.status === 200 || request.status === 201) {
            const result = request.responseText;
            let parsedResult = null;
            if (result != '') {
              parsedResult = JSON.parse(result);
            }
            resolve(parsedResult);
          } else if (request.status === 401) {
            changeState(State.AUTHORIZATION_SCREEN)
          } else if (request.status === 500) {
            reject('Deletion error: this object is associated with other entities');
          } else {
            reject('HttpService error: status = ' + request.status + "; response text = " + request.responseText);
          }
        }
      });

      const resultData = data != null ? JSON.stringify(data) : null;
      const authString = `Basic ${window.btoa(this._login + ":" + this._password)}`;
      request.setRequestHeader('Authorization', authString);
      if (resultData) {
        request.setRequestHeader('Content-Type', 'application/json');
      }
      request.send(resultData);
    });
  }

}


export default new HttpService();

