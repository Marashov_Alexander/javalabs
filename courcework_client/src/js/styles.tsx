export const menuStyle = {
    'display': 'flex',
    'flex-direction': 'column',
    'justify-content': 'center',
    'height': '-webkit-fill-available',
    'align-items': 'center'
};

export const buttonStyle = {
    'width': '40%',
    'margin-top': '1%',
    'height': '40px',
    'font-size': '20pt'
};
