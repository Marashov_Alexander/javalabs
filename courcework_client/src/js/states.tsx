import {observable} from "mobx";

export enum State {
    ERROR_SCREEN = "ERROR_SCREEN",
    MAIN_SCREEN = "MAIN_SCREEN",
    AUTHORIZATION_SCREEN = "AUTHORIZATION_SCREEN",
    LOADING_SCREEN = "LOADING_SCREEN",

    STUDENTS_ACTIONS_SCREEN = "STUDENTS_ACTIONS_SCREEN",
    TEACHERS_ACTIONS_SCREEN = "TEACHERS_ACTIONS_SCREEN",
    PEOPLE_QUERY_SCREEN = "PEOPLE_QUERY_SCREEN",

    SUBJECTS_ACTIONS_SCREEN = "SUBJECTS_ACTIONS_SCREEN",
    SUBJECTS_QUERY_SCREEN = "SUBJECTS_QUERY_SCREEN",

    MARKS_ACTIONS_SCREEN = "MARKS_ACTIONS_SCREEN",
    MARKS_QUERY_SCREEN = "MARKS_QUERY_SCREEN",

    GROUPS_ACTIONS_SCREEN = "GROUPS_ACTIONS_SCREEN",
    GROUPS_QUERY_SCREEN = "GROUPS_QUERY_SCREEN",
}

export var appState = observable({
    'state': State.AUTHORIZATION_SCREEN,
    'errorMessage': ''
});

export const changeState = (state: State) => {
    appState.state = state;
};

export const errorCaused = (error: string) => {
    appState.state = State.ERROR_SCREEN;
    appState.errorMessage = error;
};
