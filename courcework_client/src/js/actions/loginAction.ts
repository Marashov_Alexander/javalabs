import httpService from '../services/httpService';
import {changeState, errorCaused, State} from "../states";

export function loginAction(login: string, password: string, state?: State) {
  changeState(State.LOADING_SCREEN);
    httpService.login = login;
    httpService.password = password;
  httpService
    .request<String>(
      'GET',
      'http://localhost:8080/',
    )
    .then(
      str => {
          alert("Successful!");
          changeState(state === undefined ? State.MAIN_SCREEN : state);
      }
    )
    .catch(
      error => {
          errorCaused(error);
      }
    );
}
