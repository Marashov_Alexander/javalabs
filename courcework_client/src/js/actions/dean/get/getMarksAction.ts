import httpService from '../../../services/httpService';
import {changeState, errorCaused, State} from "../../../states";
import {marks, MarksItemModel} from "../../../models/MarksModel";

export function getMarksAction(studentId: number) {
  changeState(State.LOADING_SCREEN);
  httpService
    .request<MarksItemModel[]>(
      'GET',
      'http://localhost:8080/marks/by-people/' + studentId,
    )
    .then(
      list => {
          marks.items = list;
          changeState(State.MARKS_QUERY_SCREEN);
      }
    )
    .catch(
      error => {
          errorCaused("getMarksAction: " + error);
      }
    );
}
