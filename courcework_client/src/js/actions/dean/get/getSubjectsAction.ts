import httpService from '../../../services/httpService';
import {changeState, errorCaused, State} from "../../../states";
import {marks, MarksItemModel} from "../../../models/MarksModel";
import {subjects, SubjectsItemModel} from "../../../models/SubjectsModel";

export function getSubjectsAction(state?: State) {
  changeState(State.LOADING_SCREEN);
  return httpService
    .request<SubjectsItemModel[]>(
      'GET',
      'http://localhost:8080/subjects/all',
    )
    .then(
      list => {
          subjects.items = list;
          changeState(state === undefined ? State.SUBJECTS_QUERY_SCREEN : state);
          return subjects.items;
      }
    )
    .catch(
      error => {
          errorCaused("getSubjectsAction: " + error);
          return null;
      }
    );
}
