import { PeopleItemModel, people } from '../../../models/PeopleModel';
import httpService from '../../../services/httpService';
import {changeState, errorCaused, State} from "../../../states";

export function getPeopleAction(isStudents?: boolean, state?: State) {
  changeState(State.LOADING_SCREEN);
  return httpService
    .request<PeopleItemModel[]>(
      'GET',
      'http://localhost:8080/' + (isStudents === undefined ? 'people' : (isStudents ? 'students' : 'teachers')) + '/all',
    )
    .then(
      list => {
          people.items = list;
          changeState(state === undefined ? State.PEOPLE_QUERY_SCREEN : state);
      }
    )
    .catch(
      error => {
          errorCaused("getPeopleAction: " + error);
      }
    );
}

export function getStudentsByGroupAction(groupId: number) {
    changeState(State.LOADING_SCREEN);
    httpService
        .request<PeopleItemModel[]>(
            'GET',
            'http://localhost:8080/students/by-group/' + groupId,
        )
        .then(
            list => {
                people.items = list;
                changeState(State.PEOPLE_QUERY_SCREEN);
            }
        )
        .catch(
            error => {
                errorCaused("getStudentsByGroupAction: " + error);
            }
        );
}

export function getStudentsByMarkAction(minMark: number, maxMark: number) {
    changeState(State.LOADING_SCREEN);
    httpService
        .request<PeopleItemModel[]>(
            'GET',
            'http://localhost:8080/students/by-mark/?min-mark=' + minMark + '&max-mark=' + maxMark,
        )
        .then(
            list => {
                people.items = list;
                changeState(State.PEOPLE_QUERY_SCREEN);
            }
        )
        .catch(
            error => {
                errorCaused("getStudentsByMarkAction: " + error);
            }
        );
}

export function getStudentsBySubjectAction(subjectId: number) {
    changeState(State.LOADING_SCREEN);
    httpService
        .request<PeopleItemModel[]>(
            'GET',
            'http://localhost:8080/students/by-subject/' + subjectId,
        )
        .then(
            list => {
                people.items = list;
                changeState(State.PEOPLE_QUERY_SCREEN);
            }
        )
        .catch(
            error => {
                errorCaused("getStudentsBySubjectAction: " + error);
            }
        );
}
