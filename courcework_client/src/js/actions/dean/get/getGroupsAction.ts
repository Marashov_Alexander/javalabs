import httpService from '../../../services/httpService';
import {changeState, errorCaused, State} from "../../../states";
import {GroupItemModel, groups} from "../../../models/GroupsModel";

export function getGroupsAction(state?: State) {
  changeState(State.LOADING_SCREEN);
  httpService
    .request<GroupItemModel[]>(
      'GET',
      'http://localhost:8080/groups/all',
    )
    .then(
      list => {
          groups.items = list;
          changeState(state === undefined ? State.GROUPS_QUERY_SCREEN : state);
      }
    )
    .catch(
      error => {
          errorCaused("getGroupsAction: " + error);
      }
    );
}
