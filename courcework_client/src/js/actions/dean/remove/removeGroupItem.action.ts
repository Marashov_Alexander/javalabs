import httpService from '../../../services/httpService';
import {changeState, errorCaused, State} from "../../../states";
import {groups} from "../../../models/GroupsModel";

export function removeGroupItemAction(groupItemId: number) {
    changeState(State.LOADING_SCREEN);
    groups.items = groups.items.filter(item => item.id !== groupItemId);
  httpService
    .request(
      'DELETE',
      `http://localhost:8080/groups/${groupItemId}`,
    )
      .then(() => {

      })
      .catch(error => {
          alert(error)
      })
      .finally(() => {
          changeState(State.GROUPS_QUERY_SCREEN)
      });
}

