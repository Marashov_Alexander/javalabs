import { people } from '../../../models/PeopleModel';
import httpService from '../../../services/httpService';
import {changeState, errorCaused, State} from "../../../states";

export function removePeopleItemAction(peopleItemId: number) {
    changeState(State.LOADING_SCREEN);
    people.items = people.items.filter(item => item.id !== peopleItemId);
  httpService
    .request(
      'DELETE',
      `http://localhost:8080/people/${peopleItemId}`,
    )
      .then(() => {

      })
      .catch(error => {
          alert(error)
      })
      .finally(() => {
          changeState(State.PEOPLE_QUERY_SCREEN)
      });
}

