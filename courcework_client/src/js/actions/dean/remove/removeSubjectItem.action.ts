import httpService from '../../../services/httpService';
import {changeState, State} from "../../../states";
import {subjects} from "../../../models/SubjectsModel";

export function removeSubjectItemAction(subjectItemId: number) {
    changeState(State.LOADING_SCREEN);
    subjects.items = subjects.items.filter(item => item.id !== subjectItemId);
  httpService
    .request(
      'DELETE',
      `http://localhost:8080/subjects/${subjectItemId}`,
    )
      .then(() => {

      })
      .catch(error => {
          alert(error)
      }).finally(() => {
      changeState(State.SUBJECTS_QUERY_SCREEN)
  });
}

