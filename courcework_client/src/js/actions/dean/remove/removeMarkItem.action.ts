import httpService from '../../../services/httpService';
import {changeState, errorCaused, State} from "../../../states";
import {marks} from "../../../models/MarksModel";

export function removeMarkItemAction(markItemId: number) {
    changeState(State.LOADING_SCREEN);
    marks.items = marks.items.filter(item => item.id !== markItemId);
  httpService
    .request(
      'DELETE',
      `http://localhost:8080/marks/${markItemId}`,
    )
      .then(() => {

      })
      .catch(error => {
          alert(error)
      })
      .finally(() => {
          changeState(State.MARKS_QUERY_SCREEN)
      });
}

