import httpService from '../../../services/httpService';
import {changeState, errorCaused, State} from "../../../states";
import {marks, MarksItemModel} from "../../../models/MarksModel";
import {MarkItemAddingData} from "../add/addMark.action";

export function changeMarkItemAction(
    markId: number,
    markData: MarkItemAddingData
) {
    changeState(State.LOADING_SCREEN);

  httpService.request<MarksItemModel>(
    'PUT',
    `http://localhost:8080/marks/${markId}`,
    markData,
  ).then(item => {
      marks.items.find(i => {
          if (i.id === item.id) {
              i.student = item.student;
              i.subject = item.subject;
              i.teacher = item.teacher;
              i.value = item.value;
              return;
          }
      });
      changeState(State.MARKS_QUERY_SCREEN);
  }).catch(error => {
      errorCaused(error);
  });
}
