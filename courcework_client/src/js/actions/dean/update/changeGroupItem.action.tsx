import httpService from '../../../services/httpService';
import {changeState, errorCaused, State} from "../../../states";
import {GroupItemAddingData} from "../add/addGroup.action";
import {GroupItemModel, groups} from "../../../models/GroupsModel";

export function changeGroupItemAction(
    groupId: number,
    groupData: GroupItemAddingData
) {
    changeState(State.LOADING_SCREEN);

  httpService.request<GroupItemModel>(
    'PUT',
    `http://localhost:8080/groups/${groupId}`,
    groupData,
  ).then(item => {
      groups.items.find(i => {
          if (i.id === item.id) {
              i.name = item.name;
              return;
          }
      });
      changeState(State.GROUPS_QUERY_SCREEN);
  }).catch(error => {
      errorCaused(error);
  });
}
