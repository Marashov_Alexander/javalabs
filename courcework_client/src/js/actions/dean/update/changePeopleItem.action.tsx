import httpService from '../../../services/httpService';
import {PeopleItemAddingData} from "../add/addPeople.action";
import {people, PeopleItemModel} from "../../../models/PeopleModel";
import {changeState, errorCaused, State} from "../../../states";

export function changePeopleItemAction(
    peopleId: number,
    peopleData: PeopleItemAddingData
) {
    changeState(State.LOADING_SCREEN);

  httpService.request<PeopleItemModel>(
    'PUT',
    `http://localhost:8080/people/${peopleId}`,
    peopleData,
  ).then(item => {
      people.items.find(i => {
          if (i.id === item.id) {
              i.firstName = item.firstName;
              i.lastName = item.lastName;
              i.fatherName = item.fatherName;
              i.group = item.group;
              i.status = item.status;
              return;
          }
      });
      changeState(State.PEOPLE_QUERY_SCREEN);
  }).catch(error => {
      errorCaused(error);
  });
}
