import httpService from '../../../services/httpService';
import {PeopleItemAddingData} from "../add/addPeople.action";
import {people, PeopleItemModel} from "../../../models/PeopleModel";
import {changeState, errorCaused, State} from "../../../states";
import {SubjectItemAddingData} from "../add/addSubject.action";
import {subjects, SubjectsItemModel} from "../../../models/SubjectsModel";

export function changeSubjectItemAction(
    subjectId: number,
    subjectData: SubjectItemAddingData
) {
    changeState(State.LOADING_SCREEN);

  httpService.request<SubjectsItemModel>(
    'PUT',
    `http://localhost:8080/subjects/${subjectId}`,
    subjectData,
  ).then(item => {
      subjects.items.find(i => {
          if (i.id === item.id) {
              i.name = item.name;
              return;
          }
      });
      changeState(State.SUBJECTS_QUERY_SCREEN);
  }).catch(error => {
      errorCaused(error);
  });
}
