import {people, PeopleItemModel} from '../../../models/PeopleModel';
import httpService from '../../../services/httpService';
import {changeState, errorCaused, State} from "../../../states";

export interface PeopleItemAddingData {
    firstName: string,
    lastName: string,
    fatherName: string,
    groupId: string,
    status: string
}

export function addPeopleAction(peopleData: PeopleItemAddingData) {

    if (peopleData === null) {
        return;
    }

  changeState(State.LOADING_SCREEN);

  return httpService.request<PeopleItemModel>(
    'POST',
    'http://localhost:8080/people',
    peopleData
  ).then(
    newItem => {
      people.items.push(newItem);
      changeState(State.PEOPLE_QUERY_SCREEN);
      return newItem;
    }
  )
      .catch(error => {
          errorCaused(error);
      });
}
