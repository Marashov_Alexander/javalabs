import httpService from '../../../services/httpService';
import {changeState, errorCaused, State} from "../../../states";
import {subjects, SubjectsItemModel, SubjectsModel} from "../../../models/SubjectsModel";

export interface SubjectItemAddingData {
    name: string,
}

export function addSubjectAction(subjectData: SubjectItemAddingData) {

    if (subjectData === null) {
        return;
    }

  changeState(State.LOADING_SCREEN);

  return httpService.request<SubjectsItemModel>(
    'POST',
    'http://localhost:8080/subjects',
    subjectData
  ).then(
    newItem => {
      subjects.items.push(newItem);
      changeState(State.SUBJECTS_QUERY_SCREEN);
      return newItem;
    }
  )
      .catch(error => {
          errorCaused(error);
      });
}
