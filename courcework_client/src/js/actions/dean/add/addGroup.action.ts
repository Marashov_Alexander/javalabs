import httpService from '../../../services/httpService';
import {changeState, errorCaused, State} from "../../../states";
import {GroupItemModel, groups} from "../../../models/GroupsModel";

export interface GroupItemAddingData {
    name: string,
}

export function addGroupAction(groupData: GroupItemAddingData) {

    if (groupData === null) {
        return;
    }

  changeState(State.LOADING_SCREEN);

  return httpService.request<GroupItemModel>(
    'POST',
    'http://localhost:8080/groups',
    groupData
  ).then(
    newItem => {
      groups.items.push(newItem);
      changeState(State.GROUPS_QUERY_SCREEN);
      return newItem;
    }
  )
      .catch(error => {
          errorCaused(error);
      });
}
