import httpService from '../../../services/httpService';
import {changeState, errorCaused, State} from "../../../states";
import {subjects, SubjectsItemModel, SubjectsModel} from "../../../models/SubjectsModel";
import {marks, MarksItemModel} from "../../../models/MarksModel";

export interface MarkItemAddingData {
    studentId: string,
    subjectId: string,
    teacherId: string,
    value: string,
}

export function addMarkAction(markData: MarkItemAddingData) {

    if (markData === null) {
        return;
    }

  changeState(State.LOADING_SCREEN);

  return httpService.request<MarksItemModel>(
    'POST',
    'http://localhost:8080/marks',
    markData
  ).then(
    newItem => {
      marks.items.push(newItem);
      changeState(State.MARKS_QUERY_SCREEN);
      return newItem;
    }
  )
      .catch(error => {
          errorCaused(error);
      });
}
