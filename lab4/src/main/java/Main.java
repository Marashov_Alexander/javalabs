import DB.GoodsDB;

import java.sql.*;
import java.util.Scanner;

public class Main {

    private static GoodsDB goodsDB;

    public static void main(String[] args) {

        try (Connection connection = getNewConnection()) {

            goodsDB = new GoodsDB(connection);

            for (int i = 1; i <= 5; ++i) {
                goodsDB.add(i, "Good_" + i, i * 100);
            }

            String command;
            System.out.println("Welcome to goods DB!");
            showHelp();

            Scanner in = new Scanner(System.in);
            while (true) {
                command = in.nextLine();
                String[] arguments = command.split(" ");
                switch (arguments[0]) {
                    case "0":
                    case "/show_all":
                        showAll();
                        break;
                    case "1":
                    case "/add":
                        add(arguments);
                        break;
                    case "2":
                    case "/delete":
                        delete(arguments);
                        break;
                    case "3":
                    case "/price":
                        getPrice(arguments);
                        break;
                    case "4":
                    case "/change_price":
                        changePrice(arguments);
                        break;
                    case "5":
                    case "/filter_by_price":
                        filterByPrice(arguments);
                        break;
                    case "6":
                    case "/help":
                        showHelp();
                        break;
                    case "7":
                    case "/exit":
                        System.out.println("Have a nice day!");
                        return;
                    default:
                        System.out.println("Unknown command. Type '/help' to get help");
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static Connection getNewConnection() throws SQLException {
        String url = "jdbc:h2:mem:test";
        String user = "sa";
        String passwd = "sa";
        return DriverManager.getConnection(url, user, passwd);
    }

    private static void showHelp() {
        String helpString = "Please, type the command: \n" +
                "0) '/show_all' to view all goods \n" +
                "1) '/add <prodId> <title> <price>' to add a new good\n" +
                "2) '/delete <title>' to remove the good\n" +
                "3) '/price <title>' to view the good price\n" +
                "4) '/change_price <title> <new price>' to update the good price\n" +
                "5) '/filter_by_price <min price> <max price>' to get goods on such request\n" +
                "6) '/help' to get help\n" +
                "7) '/exit' to exit the program";
        System.out.println(helpString);
    }

    private static void getPrice(String[] args) {
        if (args.length != 2) {
            System.out.println("There are should be 1 parameter");
        } else {
            try {
                String goodTitle = args[1];
                int goodPrice = goodsDB.getPrice(goodTitle);
                System.out.println(goodPrice == -1 ? "No such good" : goodPrice);
            } catch (SQLException e) {
                System.out.println("No such good");
            }
        }
    }

    private static void add(String[] args) {
        if (args.length != 4) {
            System.out.println("There are should be 3 parameters");
        } else {
            try {
                int prodId = Integer.parseInt(args[1]);
                String goodTitle = args[2];
                int cost = Integer.parseInt(args[3]);

                if (cost < 1) {
                    throw new NumberFormatException();
                }

                goodsDB.add(prodId, goodTitle, cost);
                System.out.println("Ok");
            } catch (NumberFormatException e) {
                System.out.println("Cost must be a positive number");
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    private static void filterByPrice(String[] args) {
        if (args.length != 3) {
            System.out.println("There are should be 2 parameters");
        } else {
            try {
                int minCost = Integer.parseInt(args[1]);
                int maxCost = Integer.parseInt(args[2]);
                if (minCost < 1 || maxCost < 1) {
                    throw new NumberFormatException();
                }

                String result = goodsDB.getAllInCostRange(minCost, maxCost);
                System.out.println(result == null || result.equals("") ? "No such goods" : result);
            } catch (NumberFormatException e) {
                System.out.println("Min and max costs must be a positive numbers");
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    private static void changePrice(String[] args) {
        if (args.length != 3) {
            System.out.println("There are should be 2 parameters");
        } else {
            try {
                String goodTitle = args[1];
                int newCost = Integer.parseInt(args[2]);

                if (newCost < 1) {
                    throw new NumberFormatException();
                }
                System.out.println(goodsDB.changePrice(goodTitle, newCost) == 0 ? "No such good" : "Ok");

            } catch (NumberFormatException e) {
                System.out.println("New cost must be a positive number");
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    private static void showAll() {
        try {
            String result = goodsDB.getAll();
            System.out.println(result);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    private static void delete(String[] args) {
        if (args.length != 2) {
            System.out.println("There are should be 1 parameter");
        } else {
            try {
                System.out.println(goodsDB.delete(args[1]) == 0 ? "No such good" : "Ok");
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
    }
}
