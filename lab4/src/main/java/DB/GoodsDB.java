package DB;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class GoodsDB {
    private Connection connection;
    private Statement statement;

    public GoodsDB(Connection connection) throws SQLException {
        this.connection = connection;
        this.statement = connection.createStatement();
        createGoodsTable();
    }

    public int add(int prodId, String title, int cost) throws SQLException {
        checkConnection();
        String insertQuery =
                "INSERT into goods " +
                        "(prodid, title, cost) " +
                        "VALUES (" +
                        prodId + ", " +
                        "'" + title + "', " +
                        cost + " " +
                        ")";
        return executeUpdate(insertQuery);
    }

    public int delete(String title) throws SQLException {
        checkConnection();
        String deleteQuery = "DELETE goods WHERE title = '" + title + "'";
        return executeUpdate(deleteQuery);
    }

    public int getPrice(String title) throws SQLException {
        checkConnection();
        String priceQuery = "SELECT cost FROM goods WHERE title = '" + title + "'";

        if (statement.execute(priceQuery)) {
            ResultSet resultSet = statement.getResultSet();
            resultSet.next();
            try {
                return resultSet.getInt("cost");
            } catch (SQLException e) {
                return -1;
            }

        } else {
            return -1;
        }
    }

    public int changePrice(String title, int cost) throws SQLException {
        checkConnection();
        String priceQuery = "UPDATE goods SET cost = " + cost + " WHERE title = '" + title + "'";
        return executeUpdate(priceQuery);
    }

    public String getAll() throws SQLException {
        return resultSetToString(getAllGoods());
    }

    public ResultSet getAllGoods() throws SQLException {
        checkConnection();
        String query = "SELECT * FROM goods";
        if (statement.execute(query)) {
            return statement.getResultSet();
        }
        return null;
    }

    public String getAllInCostRange(int minCost, int maxCost) throws SQLException {
        return resultSetToString(getResultSetInCostRange(minCost, maxCost));
    }

    public ResultSet getResultSetInCostRange(int minCost, int maxCost) throws SQLException {
        if (minCost > maxCost) {
            throw new SQLException("Max cost mustn't be less than min cost");
        }
        checkConnection();
        String query = "SELECT * FROM goods WHERE cost >= " + minCost + " AND cost <= " + maxCost;
        if (statement.execute(query)) {
            return statement.getResultSet();
        }
        return null;
    }

    private String resultSetToString(ResultSet resultSet) throws SQLException {
        if (resultSet == null) {
            return null;
        }
        StringBuilder result = new StringBuilder();
        while (resultSet.next()) {
            result.append("Id: ").append(resultSet.getInt("id")).append("; ");
            result.append("prodid: ").append(resultSet.getInt("prodid")).append("; ");
            result.append("title: ").append(resultSet.getString("title")).append("; ");
            result.append("cost: ").append(resultSet.getInt("cost")).append("\n");
        }
        return result.toString();
    }

    private void checkConnection() throws SQLException {
        if (connection.isClosed()) {
            throw new SQLException("Connection is closed");
        }
    }

    private int executeUpdate(String query) throws SQLException {
        return statement.executeUpdate(query);
    }

    private void createGoodsTable() throws SQLException {
        checkConnection();
        String goodsTableQuery =
                "CREATE TABLE goods " +
                        "(" +
                            "id INTEGER PRIMARY KEY AUTO_INCREMENT, " +
                            "prodid INTEGER UNIQUE, " +
                            "title VARCHAR(100) UNIQUE, " +
                            "cost INTEGER" +
                        ")";
        executeUpdate(goodsTableQuery);
    }
}
