package entryPoint;

import DB.GoodsDB;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import models.GoodModel;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Main extends Application {

    public static GoodsDB goodsDB;
    public static ObservableList<GoodModel> goodsData = FXCollections.observableArrayList();

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("../views/application.fxml"));
        primaryStage.setTitle("Goods Data Base GUI");
        Scene scene = new Scene(root, 500, 500);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String[] args) {

        try (Connection connection = getNewConnection()) {

            goodsDB = new GoodsDB(connection);

            for (int i = 1; i <= 5; ++i) {
                goodsDB.add(i, "Good_" + i, i * 100);
            }

            launch(args);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static Connection getNewConnection() throws SQLException {
        String url = "jdbc:h2:mem:test";
        String user = "sa";
        String passwd = "sa";
        return DriverManager.getConnection(url, user, passwd);
    }
}
