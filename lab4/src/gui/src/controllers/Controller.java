package controllers;

import entryPoint.Main;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import models.GoodModel;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Controller {

    public Controller() {

    }

    @FXML
    private TableView<GoodModel> tableView;

    public static TableView<GoodModel> staticTableView;

    @FXML
    private TableColumn<GoodModel, Number> pidColumn;
    @FXML
    private TableColumn<GoodModel, String> titleColumn;
    @FXML
    private TableColumn<GoodModel, Number> costColumn;


    @FXML
    private Button searchButton;
    @FXML
    private Button addButton;
    @FXML
    private Button deleteButton;
    @FXML
    private Button filterButton;

    @FXML
    private void initialize() {

        try {
            ResultSet allGoods = Main.goodsDB.getAllGoods();
            while (allGoods.next()) {
                Main.goodsData.add(new GoodModel(
                        allGoods.getInt("prodid"),
                        allGoods.getString("title"),
                        allGoods.getInt("cost")
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        titleColumn.setCellValueFactory(cellData -> cellData.getValue().titleProperty());
        costColumn.setCellValueFactory(cellData -> cellData.getValue().costProperty());
        pidColumn.setCellValueFactory(cellData -> cellData.getValue().pidProperty());

        tableView.setItems(Main.goodsData);
        staticTableView = tableView;

        addButton.setOnAction(new EventHandler<>() {
            public void handle(ActionEvent event) {
                try {
                    Parent root = FXMLLoader.load(getClass().getResource("../views/add.fxml"));
                    Stage stage = new Stage();
                    stage.setTitle("Adding new good");
                    stage.setScene(new Scene(root, 450, 200));
                    stage.show();
//                    ((Node)(event.getSource())).getScene().getWindow().hide();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        searchButton.setOnAction(new EventHandler<>() {
            public void handle(ActionEvent event) {
                try {
                    Parent root = FXMLLoader.load(getClass().getResource("../views/find.fxml"));
                    Stage stage = new Stage();
                    stage.setTitle("Finding the good");
                    stage.setScene(new Scene(root, 400, 100));
                    stage.show();
//                    ((Node)(event.getSource())).getScene().getWindow().hide();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        filterButton.setOnAction(new EventHandler<>() {
            public void handle(ActionEvent event) {
                try {
                    Parent root = FXMLLoader.load(getClass().getResource("../views/filter.fxml"));
                    Stage stage = new Stage();
                    stage.setTitle("Goods filter");
                    stage.setScene(new Scene(root, 500, 200));
                    stage.show();
//                    ((Node)(event.getSource())).getScene().getWindow().hide();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        deleteButton.setOnAction(event -> {
            GoodModel selectedGood = tableView.getSelectionModel().getSelectedItem();
            if (selectedGood != null) {
                try {
                    Main.goodsDB.delete(selectedGood.getTitle());
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                Main.goodsData.remove(selectedGood);
            }

        });

    }

}
