package controllers;

import entryPoint.Main;
import javafx.collections.transformation.FilteredList;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import models.GoodModel;

public class FindController {

    public FindController() {

    }

    @FXML
    private Button okFindButton;
    @FXML
    private TextField titleFindTextArea;

    @FXML
    private void initialize() {
        okFindButton.setOnAction(event -> {
            FilteredList<GoodModel> result = Main.goodsData.filtered(goodModel -> goodModel.getTitle().equals(titleFindTextArea.getText()));
            if (result != null && !result.isEmpty()) {
                Controller.staticTableView.getSelectionModel().select(result.get(0));
            } else {
                new Alert(Alert.AlertType.INFORMATION, "No such good", ButtonType.OK).show();
            }
            ((Node)(event.getSource())).getScene().getWindow().hide();
        });
    }
}
