package controllers;

import entryPoint.Main;
import javafx.collections.transformation.FilteredList;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import models.GoodModel;

import java.sql.ResultSet;
import java.sql.SQLException;

public class FilterController {

    public FilterController() {

    }

    @FXML
    private Button resetFilterButton;
    @FXML
    private Button okFilterButton;
    @FXML
    private TextField minCostFilterTextArea;
    @FXML
    private TextField maxCostFilterTextArea;

    @FXML
    private void initialize() {
        okFilterButton.setOnAction(event -> {
            try {
                int minCost = Integer.parseInt(minCostFilterTextArea.getText());
                int maxCost = Integer.parseInt(maxCostFilterTextArea.getText());
                ResultSet resultSet = Main.goodsDB.getResultSetInCostRange(
                        minCost, maxCost
                );
                Main.goodsData.clear();
                while (resultSet.next()) {
                    Main.goodsData.add(new GoodModel(resultSet.getInt("prodid"), resultSet.getString("title"), resultSet.getInt("cost")));
                }
                ((Node)(event.getSource())).getScene().getWindow().hide();
            } catch (SQLException | NumberFormatException e) {
                new Alert(Alert.AlertType.ERROR, e.getMessage(), ButtonType.OK).show();
            }
        });

        resetFilterButton.setOnAction(event -> {
            try {
                ResultSet resultSet = Main.goodsDB.getAllGoods();
                Main.goodsData.clear();
                while (resultSet.next()) {
                    Main.goodsData.add(new GoodModel(resultSet.getInt("prodid"), resultSet.getString("title"), resultSet.getInt("cost")));
                }
                ((Node)(event.getSource())).getScene().getWindow().hide();
            } catch (SQLException e) {
                new Alert(Alert.AlertType.ERROR, e.getMessage(), ButtonType.OK).show();
            }
        });
    }
}
