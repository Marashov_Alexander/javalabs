package controllers;

import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;

import java.sql.SQLException;

import entryPoint.Main;
import models.GoodModel;

public class AddController {

    public AddController() {

    }

    @FXML
    private Button okAddButton;
    @FXML
    private TextField prodIdAddTextArea;
    @FXML
    private TextField titleAddTextArea;
    @FXML
    private TextField costAddTextArea;

    @FXML
    private void initialize() {
        okAddButton.setOnAction(actionEvent -> {
            ((Node)(actionEvent.getSource())).getScene().getWindow().hide();
            try {
                int prodId = Integer.parseInt(prodIdAddTextArea.getText());
                String title = titleAddTextArea.getText();
                int cost = Integer.parseInt(costAddTextArea.getText());
                Main.goodsDB.add(prodId, title, cost);
                Main.goodsData.add(new GoodModel(prodId, title, cost));
                ((Node)(actionEvent.getSource())).getScene().getWindow().hide();
            } catch (SQLException | NumberFormatException e) {
                new Alert(Alert.AlertType.ERROR, e.getMessage(), ButtonType.OK).show();
            }
        });
    }
}
