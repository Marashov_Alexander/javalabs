package models;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class GoodModel {
    private final IntegerProperty pid;
    private final StringProperty title;
    private final IntegerProperty cost;

    public GoodModel(int prodId, String title, int cost) {
        this.pid = new SimpleIntegerProperty(prodId);
        this.title = new SimpleStringProperty(title);
        this.cost = new SimpleIntegerProperty(cost);
    }

    public int getPid() {
        return this.pid.getValue();
    }

    public void setTitle(String title) {
        this.title.setValue(title);
        this.pid.setValue(title.hashCode());
    }

    public String getTitle() {
        return this.title.getValue();
    }

    public void setCost(int cost) {
        this.cost.setValue(cost);
    }

    public int getCost() {
        return this.cost.getValue();
    }

    public StringProperty titleProperty() {
        return title;
    }

    public IntegerProperty costProperty() {
        return cost;
    }

    public IntegerProperty pidProperty() {
        return pid;
    }
}
