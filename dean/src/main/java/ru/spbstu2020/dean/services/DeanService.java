package ru.spbstu2020.dean.services;

import ru.spbstu2020.dean.models.Marks;
import ru.spbstu2020.dean.models.People;
import ru.spbstu2020.dean.models.SGroups;
import ru.spbstu2020.dean.models.Subjects;
import ru.spbstu2020.dean.requests.MarksRequest;
import ru.spbstu2020.dean.requests.PeopleRequest;
import ru.spbstu2020.dean.requests.SGroupsRequest;
import ru.spbstu2020.dean.requests.SubjectsRequest;

import java.util.List;

public interface DeanService {
    List<People> readPeople();

    People createPeople(People people); // post: people/
    SGroups createGroup(SGroups group); // post: groups/
    Subjects createSubject(Subjects subject); // subjects/
    Marks createMark(Marks mark); // marks/

    List<People> readStudents(); // get: students/all
        List<People> readStudentsByGroup(SGroups group); // students/by-group/id
        List<People> readStudentsByMark(int minMark, int maxMark); // students/by-mark?:min-mark=,:?max-mark=
        List<People> readStudentsBySubject(Subjects subject); // students/by-subject/id
    List<People> readTeachers(); // teachers/all
    List<SGroups> readGroups(); // /groups/all
    List<Subjects> readSubjects(); // subjects/all
    List<Marks> readMarks(People student); // marks/all

    People updatePeople(PeopleRequest people, long peopleId); // update: people/id
    Subjects updateSubject(SubjectsRequest subject, long subjectId); // update: subjects/id
    SGroups updateGroup(SGroupsRequest group, long groupId); // update: groups/id
    Marks updateMark(MarksRequest mark, long markId); // update: marks/id

    void deletePeople(long id); // delete: students/id
    void deleteGroup(long id); // delete: groups/id
    void deleteMark(long id); // delete: marks/id
    void deleteSubject(long id); // delete: subjects/id
}
