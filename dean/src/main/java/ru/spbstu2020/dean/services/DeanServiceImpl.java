package ru.spbstu2020.dean.services;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import ru.spbstu2020.dean.models.Marks;
import ru.spbstu2020.dean.models.People;
import ru.spbstu2020.dean.models.SGroups;
import ru.spbstu2020.dean.models.Subjects;
import ru.spbstu2020.dean.repositories.MarksRepository;
import ru.spbstu2020.dean.repositories.PeopleRepository;
import ru.spbstu2020.dean.repositories.SGroupsRepository;
import ru.spbstu2020.dean.repositories.SubjectsRepository;
import ru.spbstu2020.dean.requests.MarksRequest;
import ru.spbstu2020.dean.requests.PeopleRequest;
import ru.spbstu2020.dean.requests.SGroupsRequest;
import ru.spbstu2020.dean.requests.SubjectsRequest;

import java.util.List;
import java.util.NoSuchElementException;

@Service
public class DeanServiceImpl implements DeanService {

    static
    SubjectsRepository subjectsRepository;
    static
    SGroupsRepository sGroupsRepository;
    static
    PeopleRepository peopleRepository;
    static
    MarksRepository marksRepository;

    public DeanServiceImpl(SubjectsRepository subjectsRepository, SGroupsRepository sGroupsRepository, PeopleRepository peopleRepository, MarksRepository marksRepository) {
        DeanServiceImpl.subjectsRepository = subjectsRepository;
        DeanServiceImpl.sGroupsRepository = sGroupsRepository;
        DeanServiceImpl.peopleRepository = peopleRepository;
        DeanServiceImpl.marksRepository = marksRepository;
    }

    @Component
    public static class StringIdToGroup implements Converter<String, SGroups> {
        @Override
        public SGroups convert(String id) {
            SGroups group;
            try {
                group = sGroupsRepository.findById(Long.parseLong(id)).get();
            } catch (NumberFormatException | NoSuchElementException e) {
                return null;
            }
            return group;
        }
    }

    @Component
    public static class StringIdToSubject implements Converter<String, Subjects> {
        @Override
        public Subjects convert(String id) {
            Subjects subject;
            try {
                subject = subjectsRepository.findById(Long.parseLong(id)).get();
            } catch (NumberFormatException | NoSuchElementException e) {
                return null;
            }
            return subject;
        }
    }

    @Component
    public static class StringIdToPeople implements Converter<String, People> {
        @Override
        public People convert(String id) {
            People people;
            try {
                people = peopleRepository.findById(Long.parseLong(id)).get();
            } catch (NumberFormatException | NoSuchElementException e) {
                return null;
            }
            return people;
        }
    }

    @Component
    public static class PeopleRequestToPeople implements Converter<PeopleRequest, People> {
        @Override
        public People convert(PeopleRequest peopleRequest) {
            People people = new People();
            people.setFirstName(peopleRequest.getFirstName());
            people.setLastName(peopleRequest.getLastName());
            people.setFatherName(peopleRequest.getFatherName());
            SGroups group = new DeanServiceImpl.StringIdToGroup().convert(peopleRequest.getGroupId());
            people.setGroup(group);
            people.setStatus(peopleRequest.getStatus());
            return people;
        }
    }

    @Component
    public static class MarksRequestToMarks implements Converter<MarksRequest, Marks> {
        @Override
        public Marks convert(MarksRequest marksRequest) {
            Marks mark = new Marks();
            People student = new StringIdToPeople().convert(marksRequest.getStudentId());
            People teacher = new StringIdToPeople().convert(marksRequest.getTeacherId());
            Subjects subject = new StringIdToSubject().convert(marksRequest.getSubjectId());
            mark.setStudent(student);
            mark.setTeacher(teacher);
            mark.setSubject(subject);
            mark.setValue(marksRequest.getValue());
            return mark;
        }
    }

    @Override
    public List<People> readPeople() {
        return peopleRepository.findAll();
    }

    @Override
    public People createPeople(People people) {
        peopleRepository.save(people);
        return people;
    }

    @Override
    public SGroups createGroup(SGroups group) {
        sGroupsRepository.save(group);
        return group;
    }

    @Override
    public Subjects createSubject(Subjects subject) {
        subjectsRepository.save(subject);
        return subject;
    }

    @Override
    public Marks createMark(Marks mark) {
        marksRepository.save(mark);
        return mark;
    }

    @Override
    public List<People> readStudents() {
        return peopleRepository.findStudentsByStatus('s');
    }

    @Override
    public List<People> readStudentsByGroup(SGroups group) {
        return peopleRepository.findStudentsByGroup(group);
    }

    @Override
    public List<People> readStudentsByMark(int minMark, int maxMark) {
        return peopleRepository.readStudentsByMark(minMark, maxMark);
    }

    @Override
    public List<People> readStudentsBySubject(Subjects subject) {
        return peopleRepository.findStudentsBySubject(subject.getId());
    }

    @Override
    public List<People> readTeachers() {
        return peopleRepository.findStudentsByStatus('t');
    }

    @Override
    public List<SGroups> readGroups() {
        return sGroupsRepository.findSGroups();
    }

    @Override
    public List<Subjects> readSubjects() {
        return subjectsRepository.findSubjects();
    }

    @Override
    public List<Marks> readMarks(People people) {
        if (people.getStatus() == 's') {
            return marksRepository.findMarksByStudent(people);
        } else {
            return marksRepository.findMarksByTeacher(people);
        }
    }

    @Override
    public People updatePeople(PeopleRequest people, long id) {
        People p = peopleRepository.findById(id).orElseThrow(NoSuchElementException::new);
        System.out.println("new: " + (people.getGroupId() == null ? "null" : people.getGroupId())
                + " | " + "old: " + (p.getGroup() == null ? "null" : p.getGroup().getId()));
        p.setFirstName(people.getFirstName());
        p.setLastName(people.getLastName());
        p.setFatherName(people.getFatherName());
        p.setStatus(people.getStatus());
        SGroups newGroup = people.getGroupId() != null
                ? sGroupsRepository.findById(Long.parseLong(people.getGroupId())).orElse(null)
                : null;
        p.setGroup(newGroup);
        peopleRepository.save(p);
        return p;
    }

    @Override
    public Subjects updateSubject(SubjectsRequest subject, long id) {
        Subjects s = subjectsRepository.findById(id).orElseThrow(NoSuchElementException::new);
        s.setName(subject.getName());
        subjectsRepository.save(s);
        return s;
    }

    @Override
    public SGroups updateGroup(SGroupsRequest group, long id) {
        SGroups g = sGroupsRepository.findById(id).orElseThrow(NoSuchElementException::new);
        g.setName(group.getName());
        sGroupsRepository.save(g);
        return g;
    }

    @Override
    public Marks updateMark(MarksRequest mark, long markId) {
        Marks m = marksRepository.findById(markId).orElseThrow(NoSuchElementException::new);
        People student = new StringIdToPeople().convert(mark.getStudentId());
        People teacher = new StringIdToPeople().convert(mark.getTeacherId());
        Subjects subject = new StringIdToSubject().convert(mark.getSubjectId());
        m.setStudent(student);
        m.setTeacher(teacher);
        m.setSubject(subject);
        m.setValue(mark.getValue());
        marksRepository.save(m);
        return m;
    }

    @Override
    public void deletePeople(long id) {
        peopleRepository.deleteById(id);
    }

    @Override
    public void deleteGroup(long id) {
        sGroupsRepository.deleteById(id);
    }

    @Override
    public void deleteMark(long id) {
        marksRepository.deleteById(id);
    }

    @Override
    public void deleteSubject(long id) {
        subjectsRepository.deleteById(id);
    }
}