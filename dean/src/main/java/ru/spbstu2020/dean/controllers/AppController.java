package ru.spbstu2020.dean.controllers;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ru.spbstu2020.dean.models.Marks;
import ru.spbstu2020.dean.models.People;
import ru.spbstu2020.dean.models.SGroups;
import ru.spbstu2020.dean.models.Subjects;
import ru.spbstu2020.dean.requests.MarksRequest;
import ru.spbstu2020.dean.requests.PeopleRequest;
import ru.spbstu2020.dean.requests.SGroupsRequest;
import ru.spbstu2020.dean.requests.SubjectsRequest;
import ru.spbstu2020.dean.services.DeanServiceImpl;

import javax.validation.Valid;
import java.util.List;

@RestController
public class AppController {

    final
    DeanServiceImpl deanService;

    public AppController(DeanServiceImpl deanService) {
        this.deanService = deanService;
    }

    @GetMapping("/")
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public String index() {
        return "{}";
    }

    @GetMapping("/marks/by-people/{people-id}")
    @ResponseBody
    public List<Marks> marks(@PathVariable(name = "people-id") People people) {
        if (people == null) {
            return null;
        }
        return deanService.readMarks(people);
    }

    @GetMapping("/subjects/all")
    @ResponseBody
    public List<Subjects> subjects() {
        return deanService.readSubjects();
    }

    @GetMapping("/groups/all")
    @ResponseBody
    public List<SGroups> groups() {
        return deanService.readGroups();
    }

    @GetMapping("/teachers/all")
    @ResponseBody
    public List<People> teachers() {
        return deanService.readTeachers();
    }

    @GetMapping("/students/all")
    @ResponseBody
    public List<People> students() {
        return deanService.readStudents();
    }

    @GetMapping("/people/all")
    @ResponseBody
    public List<People> people() {
        return deanService.readPeople();
    }

    @GetMapping("/people/{people-id}")
    @ResponseBody
    public People getPeople(@PathVariable(name = "people-id") People people) {
        return people;
    }

    @PostMapping("/people")
    @ResponseStatus(HttpStatus.CREATED)
    public People createPeople(@Valid @RequestBody PeopleRequest peopleRequest) {
        return deanService.createPeople(new DeanServiceImpl.PeopleRequestToPeople().convert(peopleRequest));
    }

    @PostMapping("/marks")
    @ResponseStatus(HttpStatus.CREATED)
    public Marks createMark(@Valid @RequestBody MarksRequest marksRequest) {
        return deanService.createMark(new DeanServiceImpl.MarksRequestToMarks().convert(marksRequest));
    }

    @PostMapping("/groups")
    @ResponseStatus(HttpStatus.CREATED)
    public SGroups createGroup(@Valid @RequestBody SGroups group) {
        return deanService.createGroup(group);
    }

    @PostMapping("/subjects")
    @ResponseStatus(HttpStatus.CREATED)
    public Subjects createSubject(@Valid @RequestBody Subjects subject) {
        return deanService.createSubject(subject);
    }

    @GetMapping("/groups/{group-id}")
    @ResponseBody
    public SGroups group(@PathVariable(name = "group-id") SGroups group) {
        return group;
    }

    @GetMapping("/students/by-group/{group-id}")
    @ResponseBody
    public List<People> studentsByGroup(@PathVariable(name = "group-id") SGroups group) {
        // нужна валидация? ничего не возвращает, если некорректный id
        if (group == null) {
            return null;
        }
        return deanService.readStudentsByGroup(group);
    }

    @GetMapping("/students/by-mark")
    @ResponseBody
    public List<People> studentsByMark(@RequestParam(name = "min-mark") int minMark, @RequestParam(name = "max-mark") int maxMark) {
        return deanService.readStudentsByMark(minMark, maxMark);
    }

    @GetMapping("/students/by-subject/{subject-id}")
    @ResponseBody
    public List<People> studentsBySubject(@PathVariable(name = "subject-id") Subjects subject) {
        // нужна валидация? ничего не возвращает, если некорректный id
        if (subject == null) {
            return null;
        }
        return deanService.readStudentsBySubject(subject);
    }

    @PutMapping("/people/{people-id}")
    @ResponseStatus(HttpStatus.OK)
    public People updatePeople(@Valid @RequestBody PeopleRequest people,
                             @PathVariable(name = "people-id") long peopleId) {
        return deanService.updatePeople(people, peopleId);
    }

    @PutMapping("/subjects/{subject-id}")
    @ResponseStatus(HttpStatus.OK)
    public Subjects updateSubject(@Valid @RequestBody SubjectsRequest subject,
                             @PathVariable(name = "subject-id") long subjectId) {
        return deanService.updateSubject(subject, subjectId);
    }

    @PutMapping("/groups/{group-id}")
    @ResponseStatus(HttpStatus.OK)
    public SGroups updateGroup(@Valid @RequestBody SGroupsRequest group,
                               @PathVariable(name = "group-id") long groupId) {
        return deanService.updateGroup(group, groupId);
    }

    @PutMapping("/marks/{mark-id}")
    @ResponseStatus(HttpStatus.OK)
    public Marks updateMark(@Valid @RequestBody MarksRequest marksRequest,
                               @PathVariable(name = "mark-id") long markId) {
        return deanService.updateMark(marksRequest, markId);
    }

    @DeleteMapping("/people/{people-id}")
    public void deletePeople(@PathVariable(name = "people-id") long id) {
        deanService.deletePeople(id);
    }

    @DeleteMapping("/groups/{group-id}")
    public void deleteGroup(@PathVariable(name = "group-id") long id) {
        deanService.deleteGroup(id);
    }

    @DeleteMapping("/subjects/{subject-id}")
    public void deleteSubject(@PathVariable(name = "subject-id") long id) {
        deanService.deleteSubject(id);
    }

    @DeleteMapping("/marks/{mark-id}")
    public void deleteMark(@PathVariable(name = "mark-id") long id) {
        deanService.deleteMark(id);
    }

}
