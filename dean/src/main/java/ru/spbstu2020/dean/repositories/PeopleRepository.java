package ru.spbstu2020.dean.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import ru.spbstu2020.dean.models.People;
import ru.spbstu2020.dean.models.SGroups;
import ru.spbstu2020.dean.models.Subjects;

import java.util.List;

public interface PeopleRepository extends CrudRepository<People, Long> {
    List<People> findStudentsByStatus(char status);
    List<People> findAll();

    List<People> findStudentsByGroup(SGroups sGroups);

    @Query(value = "SELECT * FROM people " +
            "INNER JOIN marks m ON people.id = m.student_id " +
            "WHERE people.status = 's' AND m.value <= :maxMark AND m.value >= :minMark", nativeQuery = true)
    List<People> readStudentsByMark(@Param("minMark") int minMark,
                                    @Param("maxMark") int maxMark);

    @Query(value = "SELECT * FROM people " +
            "INNER JOIN marks m ON people.id = m.student_id " +
            "WHERE people.status = 's' AND m.subjects_id = :subjectId", nativeQuery = true)
    List<People> findStudentsBySubject(@Param("subjectId") long subjectId);
}
