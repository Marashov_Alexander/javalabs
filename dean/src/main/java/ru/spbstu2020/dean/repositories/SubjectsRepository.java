package ru.spbstu2020.dean.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import ru.spbstu2020.dean.models.Subjects;

import java.util.List;

public interface SubjectsRepository extends CrudRepository<Subjects, Long> {

    @Query(value = "SELECT * FROM subjects as s", nativeQuery = true)
    List<Subjects> findSubjects();
}
