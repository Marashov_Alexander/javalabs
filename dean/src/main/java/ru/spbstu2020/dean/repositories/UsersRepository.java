package ru.spbstu2020.dean.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import ru.spbstu2020.dean.models.People;
import ru.spbstu2020.dean.models.SGroups;
import ru.spbstu2020.dean.models.Users;

import java.util.List;

public interface UsersRepository extends CrudRepository<Users, Long> {
    Users findByUsername(String username);
}
