package ru.spbstu2020.dean.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import ru.spbstu2020.dean.models.SGroups;

import java.util.List;

public interface SGroupsRepository extends CrudRepository<SGroups, Long> {

    @Query(value = "SELECT * FROM sgroups", nativeQuery = true)
    List<SGroups> findSGroups();
}
