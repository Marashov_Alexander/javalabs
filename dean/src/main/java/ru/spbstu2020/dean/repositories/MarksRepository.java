package ru.spbstu2020.dean.repositories;

import org.springframework.data.repository.CrudRepository;
import ru.spbstu2020.dean.models.Marks;
import ru.spbstu2020.dean.models.People;

import java.util.List;

public interface MarksRepository extends CrudRepository<Marks, Long> {

    List<Marks> findMarksByStudent(People student);
    List<Marks> findMarksByTeacher(People teacher);
}
