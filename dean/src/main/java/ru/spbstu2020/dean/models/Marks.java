package ru.spbstu2020.dean.models;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "marks", schema = "dean")
public class Marks {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
//    @JsonIgnore
    private Long id;


    @NotNull
    @ManyToOne
    @JoinColumn(name = "student_id")
    private People student;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "subjects_id")
    private Subjects subject;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "teacher_id")
    private People teacher;

    @NotNull
    @Min(1)
    @Max(5)
    @Column(name = "value")
    private Integer value;

    public Marks() {

    }

//    public Marks(long id, People student, Subjects subject, People teacher, Integer value) {
//        this.id = id;
//        this.student = student;
//        this.subject = subject;
//        this.teacher = teacher;
//        this.value = value;
//    }
//
//    public Marks(People student, Subjects subject, People teacher, Integer value) {
//        this.student = student;
//        this.subject = subject;
//        this.teacher = teacher;
//        this.value = value;
//    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Subjects getSubject() {
        return subject;
    }

    public void setSubject(Subjects subject) {
        this.subject = subject;
    }

    public People getStudent() {
        return student;
    }

    public void setStudent(People student) {
        this.student = student;
    }

    public People getTeacher() {
        return teacher;
    }

    public void setTeacher(People teacher) {
        this.teacher = teacher;
    }

    public int getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }
}
