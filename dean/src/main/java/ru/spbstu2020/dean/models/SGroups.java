package ru.spbstu2020.dean.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "sgroups", schema = "dean")
public class SGroups {

    @Id
    @Column(name = "id")
//    @JsonIgnore
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "name")
    private String name;

//    @OneToMany(mappedBy = "group", cascade = CascadeType.ALL)
//    private Set<People> peopleSet;

    public SGroups() {

    }

    public SGroups(long id, String name) {
        this.id = id;
        this.name = name;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
