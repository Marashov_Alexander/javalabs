package ru.spbstu2020.dean.requests;

import javax.validation.constraints.NotNull;

public class SGroupsRequest {
    @NotNull
    private String name;

    public SGroupsRequest() {

    }

    public SGroupsRequest(String name) {
        this.name = name;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
