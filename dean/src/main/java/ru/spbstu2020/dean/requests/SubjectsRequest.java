package ru.spbstu2020.dean.requests;


public class SubjectsRequest {

    private String name;

    public SubjectsRequest() {

    }

    public SubjectsRequest(String name) {
        this.name = name;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
