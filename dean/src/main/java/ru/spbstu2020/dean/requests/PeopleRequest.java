package ru.spbstu2020.dean.requests;

import javax.validation.constraints.NotNull;

public class PeopleRequest {

    @NotNull
    private String firstName;
    @NotNull
    private String lastName;

    private String fatherName;

    private String groupId;

    @NotNull
    private Character status;

    public PeopleRequest() {

    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public char getStatus() {
        return status;
    }

    public void setStatus(Character status) {
        this.status = status;
    }
}
