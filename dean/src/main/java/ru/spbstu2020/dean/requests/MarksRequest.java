package ru.spbstu2020.dean.requests;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class MarksRequest {

    @NotNull
    private String studentId;
    @NotNull
    private String subjectId;
    @NotNull
    private String teacherId;
    @NotNull
    @Min(1)
    @Max(5)
    private Integer value;

    public MarksRequest() {

    }

    public MarksRequest(String studentId, String subjectId, String teacherId, Integer value) {
        this.studentId = studentId;
        this.subjectId = subjectId;
        this.teacherId = teacherId;
        this.value = value;
    }

    public String getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }
}
